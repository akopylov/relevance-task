import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

export default new Vuex.Store(
    {
        state: {
            page: 1,
            ontology_file: null,
            document_files: [],
            document_files_parsed: [],
            ontology_file_parsed: null,
            document_files_cleaned: [],
            loading: false,
            document_files_ranged: [],
            n: 10
        },
        mutations: {
            setLoading(state, loading) {
                state.loading = loading;
            },
            nextPage(state) {
                if (state.page <= 2 && state.page !== 1)
                    state.page += 1;
                else if (state.page === 1) {
                    state.loading = true;

                    let parsed = [];
                    for (let file of state.document_files) {
                        let reader = new FileReader();
                        reader.readAsText(file);
                        reader.onload = function (evt) {
                            parsed.push(evt.target.result);
                        }
                    }

                    state.document_files_parsed = parsed;
                    state.loading = false;
                    state.page += 1;
                }
            },
            prevPage(state) {
                if (state.page > 1)
                    state.page -= 1;
            },
            setOntologyFile(state, file) {
                state.loading = true;

                state.ontology_file = file;
                if (file !== null) {
                    let reader = new FileReader();
                    reader.readAsText(file);
                    reader.onload = function (evt) {
                        state.ontology_file_parsed = JSON.parse(evt.target.result);
                        state.loading = false;
                    }
                } else {
                    state.ontology_file_parsed = null;
                }
            },
            setDocumentFiles(state, files) {
                state.document_files = files;
            },
            setCleanedDocuments(state, files) {
                state.document_files_cleaned = files;
            },
            setRangedDocuments(state, files) {
                state.document_files_ranged = files;
            },
            setN(state) {
                return state.N
            },
            resetChanges(state) {
                Object.assign(
                    state,
                    {
                        page: 1,
                        ontology_file: null,
                        document_files: [],
                        document_files_parsed: [],
                        ontology_file_parsed: null,
                        document_files_cleaned: [],
                        loading: false,
                        document_files_ranged: [],
                        n: 10
                    }
                )
            }
        },
        actions: {
        },
        modules: {
        },
        plugins: [createPersistedState()],
        getters: {
            getPage(state) {
                return state.page;
            },
            getLoading(state) {
                return state.loading;
            },
            getParsedTexts(state) {
                return state.document_files_parsed;
            },
            getCleanedTexts(state) {
                return state.document_files_cleaned;
            },
            getDocumentFiles(state) {
                return state.document_files;
            },
            getRangedDocuments(state) {
                return state.document_files_ranged;
            },
            getN(state) {
                return state.N
            },
            getParsedOntologyFile(state) {
                return state.ontology_file_parsed
            },
            getOntologyFile(state) {
                return state.ontology_file
            },
        }
    }
)