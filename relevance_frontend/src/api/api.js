import axios from 'axios'

export default() => {
    let host = 'localhost';
    let port = '5075';
    return axios.create({
        baseURL: `http://${host}:${port}`,
    })
}