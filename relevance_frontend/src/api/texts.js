import Api from './api'

export default {
    reduce(text, ontology) {
        return Api().post(`/reduce`, {
            raw_text: text,
            ontology
        })
    },
    proximity(texts, k, n, beta, ontology) {
        return Api().post(`/proximity`, {
            documents: texts,
            k,
            n,
            beta,
            ontology
        })
    },
}