import pymorphy2
import re
from ontology import Ontology
from util import get_unique_pairs, calc_proximity

class Document:
    def __init__(self, text=None, path=None):
        if text is None and path is None:
            raise Exception('path or text should be specified')
        if text is not None and path is not None:
            raise Exception('path OR text should be specified, not both')

        if path is not None:
            text = Document.read_file(path)

        text = Document.preprocess_text(text)
        text = Document.morph_words(text)
        self.sentences = [x.strip() for x in text.split('. ') if len(x)]
        self.raw_text = text
        self.storage = {}

    @staticmethod
    def read_file(path):
        # Read input text
        input_file = open(path, 'r', encoding='utf-8')
        input_text = input_file.read()
        input_file.close()

        return input_text

    @staticmethod
    def preprocess_text(input_text):
        # Preprocess text
        input_text = input_text.lower()
        input_text = input_text.replace('\n', ' ')

        input_text = ''.join(x if x.isalpha() or x in ' 1234567890.-' else ' ' for x in input_text)

        input_text = re.sub(r'  +', ' ', input_text)  # Remove repeated spaces
        input_text = input_text.strip()

        return input_text

    @staticmethod
    def morph_words(text):
        morph_analyzer = pymorphy2.MorphAnalyzer()
        normalized = [morph_analyzer.parse(x)[0].normal_form for x in text.split(' ')]

        return ' '.join(normalized)

    def process_sentences(self, keys):
        for sentence in self.sentences:
            storage_entry = {'occurrences': []}

            for key in keys:
                if key['name'] in sentence:
                    storage_entry['occurrences'].append(key['id'])

            if len(storage_entry['occurrences']):
                self.storage[self.sentences.index(sentence)] = storage_entry

    def get_occurrences(self):
        terms_list = []

        for index in self.storage.keys():
            terms_list.extend(self.storage[index]['occurrences'])

        unique = list(set(terms_list))
        unique.sort(key=lambda x: int(x))

        return unique

    def filter_out_pairs(self, pairs, k_parameter):
        close_pairs = {}

        for pair in pairs:
            close_pairs[pair] = 0
            last_seen_first = None
            last_seen_second = None

            for key in self.storage.keys():
                element_encountered = False

                if pair[0] in self.storage[key]['occurrences']:
                    last_seen_first = key
                    element_encountered = True
                if pair[1] in self.storage[key]['occurrences']:
                    last_seen_second = key
                    element_encountered = True

                # if both of the elements have been encountered
                # and at least one was encountered at this step
                process_pair = element_encountered and last_seen_second is not None and last_seen_first is not None

                if process_pair and abs(last_seen_second - last_seen_first) <= k_parameter:
                    close_pairs[pair] += 1

        return close_pairs

    def get_reduced(self):
        return [x for x in self.sentences if self.sentences.index(x) in self.storage.keys()]


if __name__ == '__main__':
    onto = Ontology(path='files/onto/neuro.ont')
    doc = Document(path='files/4.txt')

    doc.process_sentences(onto.get_node_list())
    occ = doc.get_occurrences()
    print(occ)
    print(onto.get_node_list())

    new_onto = onto.get_derived(doc.get_occurrences(), 10)
    new_onto.save_as('onto_der.ont')

    pairs = get_unique_pairs(occ)
    print(pairs)
    print(doc.storage)
    filtered_pairs = doc.filter_out_pairs(pairs, 5)
    print(filtered_pairs)

    print(calc_proximity(filtered_pairs, new_onto, 2))
    print(doc.get_reduced())
