from flask import Flask, request, make_response, jsonify
from document import Document
from ontology import Ontology
import util
import json

app = Flask(
    __name__
)

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', 'http://localhost:8080')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  response.headers.add('Access-Control-Allow-Credentials', 'true')
  return response

@app.route('/', methods=['GET'])
def homepage():
    resp = make_response()
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/reduce', methods=['POST', 'OPTIONS'])
def get_url():
    try:
        request_data = request.get_json(force=False, silent=False, cache=True)

        if request_data['ontology'] is None:
            onto = Ontology(path='files/onto/neuro.ont')
        else:
            onto = Ontology(source=request_data['ontology'])

        doc = Document(text=request_data['raw_text'])

        doc.process_sentences(onto.get_node_list())

        print(doc.sentences)

        response = make_response({'reduced': doc.get_reduced(), 'msg': 'ok'})
        return response

    except KeyError:
        response = make_response({'url': None, 'msg': 'Required key "raw_text" is missing'})
        return response
    except TypeError:
        response = make_response({'url': None, 'msg': 'Incorrect data payload'})
        return response
    except:
        response = make_response({'url': None, 'msg': 'Unknown error'})
        return response

@app.route('/proximity', methods=['POST', 'OPTIONS'])
def calc_proximity():
    try:
        request_data = request.get_json(force=False, silent=False, cache=True)

        if request_data['ontology'] is None:
            onto = Ontology(path='files/onto/neuro.ont')
        else:
            onto = Ontology(source=request_data['ontology'])

        results = []
        for document in request_data['documents']:
            doc = Document(text=document['raw_text'])
            doc.process_sentences(onto.get_node_list())

            occ = doc.get_occurrences()

            new_onto = onto.get_derived(doc.get_occurrences(), request_data['n'])
            pairs = util.get_unique_pairs(occ)
            filtered_pairs = doc.filter_out_pairs(pairs, request_data['k'])

            results.append({
                'name': document['name'],
                'ontology': new_onto.source,
                'r': util.calc_proximity(filtered_pairs, new_onto, request_data['beta'])
            })

        response = make_response({'processed': results, 'msg': 'ok'})
        return response

    except KeyError:
        response = make_response({'url': None, 'msg': 'Required key is missing'})
        return response
    except TypeError:
        response = make_response({'url': None, 'msg': 'Incorrect data payload'})
        return response
    except:
        response = make_response({'url': None, 'msg': 'Unknown error'})
        return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5075)
