import json
from util import flat_out_list
import pymorphy2

class Ontology:
    def __init__(self, path=None, source=None):
        if source is None and path is None:
            raise Exception('path or source should be specified')
        if source is not None and path is not None:
            raise Exception('path OR source should be specified, not both')

        if path is None:
            self.source = source
        else:
            self.source = json.load(open(path, encoding='utf-8-sig'))

    def get_node_list(self):
        morph_analyzer = pymorphy2.MorphAnalyzer()
        return [
            {
                'name': morph_analyzer.parse(x['name'].lower())[0].normal_form,
                'id': x['id']
            }
            for x in self.source['nodes']
        ]

    def find_paths_between(self, start_id, end_id, n_parameter):
        paths = self._find_path_rec(start_id, end_id)
        return [p for p in paths if len(p) <= n_parameter]

    # Dijkstra's algorithm
    def _find_path_rec(self, start_id, end_id, path=[]):
        path = path + [start_id]

        if start_id == end_id:
            return [path]

        paths = []
        for relation in self.source['relations']:
            rel_nodes = [relation['destination_node_id'], relation['source_node_id']]
            if start_id in rel_nodes:
                second_node = [x for x in rel_nodes if x != start_id][0]

                if second_node not in path:
                    new_paths = self._find_path_rec(second_node, end_id, path)

                    for new_path in new_paths:
                        paths.append(new_path)

        return paths

    def get_derived(self, indices, n_parameter):
        nodes_between = []

        # KISS: keep it simple and stupid
        # I know, it's ineffective
        for i in indices:
            for j in indices:
                paths = self.find_paths_between(i, j, n_parameter)
                nodes_between.extend(flat_out_list(paths))

        nodes_to_keep = list(set(nodes_between))

        new_source = self.source.copy()

        # Keep only nodes between
        new_source['nodes'] = [x for x in new_source['nodes'] if x['id'] in nodes_to_keep]

        # Keep only relations between existing nodes
        new_source['relations'] = [
            relation
            for relation in new_source['relations']
            if relation['destination_node_id'] in nodes_to_keep and relation['source_node_id'] in nodes_to_keep
        ]
        return Ontology(source=new_source)

    def save_as(self, path):
        with open(path, 'w', encoding='utf8') as fp:
            json.dump(self.source, fp, ensure_ascii=False, indent=4)
