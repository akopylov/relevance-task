from itertools import combinations
from math import sqrt

def flat_out_list(lst):
    return list(set([item for sublist in lst for item in sublist]))

def get_unique_pairs(lst):
    return [comb for comb in combinations(lst, 2)]

def calc_proximity(filtered_pairs, ontology, beta):
    # Raw pair proximity
    raw_pair_proximity = []

    for pair in filtered_pairs.keys():
        # Params according to formula
        i = pair[0]
        j = pair[1]

        # p - Количество различных путей в онтологии между понятиями
        p = len(ontology.find_paths_between(i, j, 999))

        # b_i - количество дуг весом, больше beta понятия i
        # (это количество таких найденных пар в тексте, удовлетворяющих расстоянию k,
        # в которых содержится понятие i, количество экземпляров которых больше beta)
        b_i = len([
            filtered_pairs[x]
            for x in filtered_pairs.keys()
            if filtered_pairs[x] > beta and i in x and pair != x
        ])

        # b_j - количество дуг весом, больше beta понятия j
        # (это количество таких найденных пар в тексте, удовлетворяющих расстоянию k,
        # в которых содержится понятие j, количество экземпляров которых больше beta)
        b_j = len([
            filtered_pairs[x]
            for x in filtered_pairs.keys()
            if filtered_pairs[x] > beta and j in x and pair != x
        ])

        # среднее число связанных с текущим понятием
        # (количество пар, в которых встречается понятие ci, поделённое
        # на общее количество в терминов в документе)
        pairs_with_i = len([
            filtered_pairs[x]
            for x in filtered_pairs.keys()
            if i in x
        ])
        mu = float(pairs_with_i)/float(len(filtered_pairs.keys()))

        # e_ij - кол-во конкордансов, где встретились оба понятия i, j
        # (количество экземпляров конкретно пары понятий i, j)
        e_ij = filtered_pairs[pair]

        numerator = 2 * mu * e_ij  # числитель дроби
        denominator = b_i + b_j  # знаменатель дроби

        if numerator == 0 or denominator == 0 or p == 0:
            raw_pair_proximity.append(0)
        else:
            raw_proximity = sqrt(p) * float(numerator)/float(denominator)
            raw_pair_proximity.append(raw_proximity)

    if max(raw_pair_proximity) == 0:
        return 0

    scaled_pair_proximity = [
        float(x - min(raw_pair_proximity))/(max(raw_pair_proximity))
        for x in raw_pair_proximity
    ]

    r = float(1)/len(filtered_pairs) * sum(scaled_pair_proximity)

    return r
